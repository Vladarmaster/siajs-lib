"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ava_1 = __importDefault(require("ava"));
const address_1 = require("./address");
ava_1.default('should return true for valid siacoin addresses', t => {
    t.true(address_1.isValidAddress('a9b01c85163638682b170d82de02b8bb99ba86092e9ab1b0d25111284fe618e93456915820f1'));
    t.true(address_1.isValidAddress('ab0c327982abfcc6055a6c9551589167d8a73501aca8769f106371fbc937ad100c955c3b7ba9'));
    t.true(address_1.isValidAddress('ffe1308c044ade30392a0cdc1fd5a4dbe94f9616a95faf888ed36123d9e711557aa497530373'));
});
ava_1.default('should return false for invalid siacoin addresses', t => {
    t.false(address_1.isValidAddress('bNEMVqeUZUqTrYUxud5ehnUhtTAiWDXQ5e'));
    t.false(address_1.isValidAddress('0xa0859a061c1bc863ebafe49203dca8196c7deba26aa7f86be80728423e8da21c'));
    t.false(address_1.isValidAddress('1A1zP1ePQGefi2DMPTifTL5SLmv7DivfNa'));
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWRkcmVzcy5zcGVjLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2xpYi9hZGRyZXNzLnNwZWMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSw4Q0FBdUI7QUFDdkIsdUNBQTJDO0FBRTNDLGFBQUksQ0FBQyxnREFBZ0QsRUFBRSxDQUFDLENBQUMsRUFBRTtJQUN6RCxDQUFDLENBQUMsSUFBSSxDQUNKLHdCQUFjLENBQ1osOEVBQThFLENBQy9FLENBQ0YsQ0FBQztJQUNGLENBQUMsQ0FBQyxJQUFJLENBQ0osd0JBQWMsQ0FDWiw4RUFBOEUsQ0FDL0UsQ0FDRixDQUFDO0lBQ0YsQ0FBQyxDQUFDLElBQUksQ0FDSix3QkFBYyxDQUNaLDhFQUE4RSxDQUMvRSxDQUNGLENBQUM7QUFDSixDQUFDLENBQUMsQ0FBQztBQUVILGFBQUksQ0FBQyxtREFBbUQsRUFBRSxDQUFDLENBQUMsRUFBRTtJQUM1RCxDQUFDLENBQUMsS0FBSyxDQUFDLHdCQUFjLENBQUMsb0NBQW9DLENBQUMsQ0FBQyxDQUFDO0lBQzlELENBQUMsQ0FBQyxLQUFLLENBQ0wsd0JBQWMsQ0FDWixvRUFBb0UsQ0FDckUsQ0FDRixDQUFDO0lBQ0YsQ0FBQyxDQUFDLEtBQUssQ0FBQyx3QkFBYyxDQUFDLG9DQUFvQyxDQUFDLENBQUMsQ0FBQztBQUNoRSxDQUFDLENBQUMsQ0FBQyJ9