import { BigNumber } from 'bignumber.js';
export declare function toSiacoins(hastings: BigNumber | number): BigNumber;
export declare function toHastings(siacoins: BigNumber | number): BigNumber;
