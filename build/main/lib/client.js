"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const child_process_1 = require("child_process");
const fs_1 = __importDefault(require("fs"));
const http_1 = __importDefault(require("http"));
const request_promise_native_1 = __importDefault(require("request-promise-native"));
const flags_1 = require("./flags");
const utils_1 = require("./utils");
class Client {
    constructor(config = {}) {
        // Set spawn to public because of the need for sinon stubbing, not sure if
        // there's a better way.
        this.spawn = child_process_1.spawn;
        this.launch = (binPath) => {
            try {
                // Check if siad exists
                if (fs_1.default.existsSync(binPath)) {
                    // Create flags
                    const flags = flags_1.parseFlags(this.config);
                    // Set euid if avl
                    const opts = {};
                    if (process.geteuid) {
                        opts.uid = process.geteuid();
                    }
                    this.process = this.spawn(binPath, flags, opts);
                    return this.process;
                }
                else {
                    throw new Error('could not find binary file in filesystem');
                }
            }
            catch (e) {
                throw new Error(e);
            }
        };
        this.makeRequest = (endpoint, querystring, method = 'GET', timeout = 30000) => __awaiter(this, void 0, void 0, function* () {
            try {
                const requestOptions = this.mergeDefaultRequestOptions({
                    url: endpoint,
                    timeout,
                    qs: querystring,
                    method
                });
                const data = yield request_promise_native_1.default(requestOptions);
                return data;
            }
            catch (e) {
                throw new Error(e);
            }
        });
        this.call = (options) => {
            if (typeof options === 'string') {
                return this.makeRequest(options);
            }
            else {
                const endpoint = options.url;
                const method = options.method;
                const qs = options.qs || undefined;
                return this.makeRequest(endpoint, qs, method);
            }
        };
        this.gateway = () => {
            return this.makeRequest('/gateway');
        };
        this.daemonVersion = () => {
            return this.makeRequest('/daemon/version');
        };
        this.daemonStop = () => {
            return this.makeRequest('/daemon/stop');
        };
        /**
         * checks if siad responds to a /version call.
         */
        this.isRunning = () => __awaiter(this, void 0, void 0, function* () {
            if (this.process) {
                try {
                    yield this.daemonVersion();
                    return true;
                }
                catch (e) {
                    throw new Error(`launched process ${this.process.pid} but not connectable`);
                }
            }
            else {
                try {
                    yield this.daemonStop();
                    return true;
                }
                catch (e) {
                    throw new Error('unable to reach sia daemon');
                }
            }
        });
        this.getConnectionUrl = () => {
            return `http://:${this.config.apiAuthenticationPassword}@${this.config.apiHost}:${this.config.apiPort}`;
        };
        this.mergeDefaultRequestOptions = (opts) => {
            // These are the default config sourced from the Sia Agent
            const defaultOptions = {
                baseUrl: this.getConnectionUrl(),
                headers: {
                    'User-Agent': this.config.agent || 'SiaPrime-Agent'
                },
                json: true,
                pool: this.agent,
                timeout: 10000
            };
            const formattedOptions = Object.assign({}, defaultOptions, opts);
            return formattedOptions;
        };
        try {
            if (config.dataDirectory) {
                fs_1.default.existsSync(config.dataDirectory);
            }
            const defaultConfig = {
                apiAuthentication: 'auto',
                apiHost: 'localhost',
                apiPort: 4280,
                hostPort: 4282,
                rpcPort: 4281
            };
            this.config = Object.assign({}, defaultConfig, config);
            // If strategy is set to 'auto', attempt to read from default siapassword file.
            if (this.config.apiAuthentication === 'auto') {
                this.config.apiAuthenticationPassword = utils_1.getSiaPassword();
            }
            this.agent = new http_1.default.Agent({
                keepAlive: true,
                maxSockets: 30
            });
        }
        catch (e) {
            throw new Error(e);
        }
    }
}
exports.Client = Client;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xpZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2xpYi9jbGllbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQUFBLGlEQUFvRDtBQUNwRCw0Q0FBb0I7QUFDcEIsZ0RBQXdCO0FBRXhCLG9GQUF3QztBQUV4QyxtQ0FBcUM7QUFFckMsbUNBQXlDO0FBRXpDLE1BQWEsTUFBTTtJQVFqQixZQUFZLFNBQXVCLEVBQUU7UUFQckMsMEVBQTBFO1FBQzFFLHdCQUF3QjtRQUNqQixVQUFLLEdBQUcscUJBQUssQ0FBQztRQStCZCxXQUFNLEdBQUcsQ0FBQyxPQUFlLEVBQWdCLEVBQUU7WUFDaEQsSUFBSTtnQkFDRix1QkFBdUI7Z0JBQ3ZCLElBQUksWUFBRSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsRUFBRTtvQkFDMUIsZUFBZTtvQkFDZixNQUFNLEtBQUssR0FBRyxrQkFBVSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFDdEMsa0JBQWtCO29CQUNsQixNQUFNLElBQUksR0FBUSxFQUFFLENBQUM7b0JBQ3JCLElBQUksT0FBTyxDQUFDLE9BQU8sRUFBRTt3QkFDbkIsSUFBSSxDQUFDLEdBQUcsR0FBRyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUM7cUJBQzlCO29CQUVELElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO29CQUNoRCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7aUJBQ3JCO3FCQUFNO29CQUNMLE1BQU0sSUFBSSxLQUFLLENBQUMsMENBQTBDLENBQUMsQ0FBQztpQkFDN0Q7YUFDRjtZQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUNWLE1BQU0sSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDcEI7UUFDSCxDQUFDLENBQUM7UUFFSyxnQkFBVyxHQUFHLENBQ25CLFFBQXNCLEVBQ3RCLFdBQWdDLEVBQ2hDLFNBQWlCLEtBQUssRUFDdEIsVUFBa0IsS0FBSyxFQUN2QixFQUFFO1lBQ0YsSUFBSTtnQkFDRixNQUFNLGNBQWMsR0FBRyxJQUFJLENBQUMsMEJBQTBCLENBQUM7b0JBQ3JELEdBQUcsRUFBRSxRQUFRO29CQUNiLE9BQU87b0JBQ1AsRUFBRSxFQUFFLFdBQVc7b0JBQ2YsTUFBTTtpQkFDUCxDQUFDLENBQUM7Z0JBQ0gsTUFBTSxJQUFJLEdBQUcsTUFBTSxnQ0FBRSxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUN0QyxPQUFPLElBQUksQ0FBQzthQUNiO1lBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ1YsTUFBTSxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNwQjtRQUNILENBQUMsQ0FBQSxDQUFDO1FBRUssU0FBSSxHQUFHLENBQUMsT0FBbUMsRUFBRSxFQUFFO1lBQ3BELElBQUksT0FBTyxPQUFPLEtBQUssUUFBUSxFQUFFO2dCQUMvQixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDbEM7aUJBQU07Z0JBQ0wsTUFBTSxRQUFRLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQztnQkFDN0IsTUFBTSxNQUFNLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQztnQkFDOUIsTUFBTSxFQUFFLEdBQUcsT0FBTyxDQUFDLEVBQUUsSUFBSSxTQUFTLENBQUM7Z0JBQ25DLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUUsRUFBRSxFQUFFLE1BQU0sQ0FBQyxDQUFDO2FBQy9DO1FBQ0gsQ0FBQyxDQUFDO1FBRUssWUFBTyxHQUFHLEdBQUcsRUFBRTtZQUNwQixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDdEMsQ0FBQyxDQUFDO1FBRUssa0JBQWEsR0FBRyxHQUFHLEVBQUU7WUFDMUIsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLENBQUM7UUFDN0MsQ0FBQyxDQUFDO1FBRUssZUFBVSxHQUFHLEdBQUcsRUFBRTtZQUN2QixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDMUMsQ0FBQyxDQUFDO1FBRUY7O1dBRUc7UUFDSSxjQUFTLEdBQUcsR0FBMkIsRUFBRTtZQUM5QyxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7Z0JBQ2hCLElBQUk7b0JBQ0YsTUFBTSxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7b0JBQzNCLE9BQU8sSUFBSSxDQUFDO2lCQUNiO2dCQUFDLE9BQU8sQ0FBQyxFQUFFO29CQUNWLE1BQU0sSUFBSSxLQUFLLENBQ2Isb0JBQW9CLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxzQkFBc0IsQ0FDM0QsQ0FBQztpQkFDSDthQUNGO2lCQUFNO2dCQUNMLElBQUk7b0JBQ0YsTUFBTSxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7b0JBQ3hCLE9BQU8sSUFBSSxDQUFDO2lCQUNiO2dCQUFDLE9BQU8sQ0FBQyxFQUFFO29CQUNWLE1BQU0sSUFBSSxLQUFLLENBQUMsNEJBQTRCLENBQUMsQ0FBQztpQkFDL0M7YUFDRjtRQUNILENBQUMsQ0FBQSxDQUFDO1FBRUsscUJBQWdCLEdBQUcsR0FBVyxFQUFFO1lBQ3JDLE9BQU8sV0FBVyxJQUFJLENBQUMsTUFBTSxDQUFDLHlCQUF5QixJQUNyRCxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQ2QsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQzVCLENBQUMsQ0FBQztRQUVNLCtCQUEwQixHQUFHLENBQ25DLElBQXVCLEVBQ0osRUFBRTtZQUNyQiwwREFBMEQ7WUFDMUQsTUFBTSxjQUFjLEdBQXdCO2dCQUMxQyxPQUFPLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixFQUFFO2dCQUNoQyxPQUFPLEVBQUU7b0JBQ1AsWUFBWSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxJQUFJLGdCQUFnQjtpQkFDcEQ7Z0JBQ0QsSUFBSSxFQUFFLElBQUk7Z0JBQ1YsSUFBSSxFQUFFLElBQUksQ0FBQyxLQUFLO2dCQUNoQixPQUFPLEVBQUUsS0FBSzthQUNmLENBQUM7WUFDRixNQUFNLGdCQUFnQixxQkFBUSxjQUFjLEVBQUssSUFBSSxDQUFFLENBQUM7WUFDeEQsT0FBTyxnQkFBZ0IsQ0FBQztRQUMxQixDQUFDLENBQUM7UUF0SUEsSUFBSTtZQUNGLElBQUksTUFBTSxDQUFDLGFBQWEsRUFBRTtnQkFDeEIsWUFBRSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUM7YUFDckM7WUFDRCxNQUFNLGFBQWEsR0FBaUI7Z0JBQ2xDLGlCQUFpQixFQUFFLE1BQU07Z0JBQ3pCLE9BQU8sRUFBRSxXQUFXO2dCQUNwQixPQUFPLEVBQUUsSUFBSTtnQkFDYixRQUFRLEVBQUUsSUFBSTtnQkFDZCxPQUFPLEVBQUUsSUFBSTthQUNkLENBQUM7WUFDRixJQUFJLENBQUMsTUFBTSxxQkFBUSxhQUFhLEVBQUssTUFBTSxDQUFFLENBQUM7WUFDOUMsK0VBQStFO1lBQy9FLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsS0FBSyxNQUFNLEVBQUU7Z0JBQzVDLElBQUksQ0FBQyxNQUFNLENBQUMseUJBQXlCLEdBQUcsc0JBQWMsRUFBRSxDQUFDO2FBQzFEO1lBQ0QsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLGNBQUksQ0FBQyxLQUFLLENBQUM7Z0JBQzFCLFNBQVMsRUFBRSxJQUFJO2dCQUNmLFVBQVUsRUFBRSxFQUFFO2FBQ2YsQ0FBQyxDQUFDO1NBQ0o7UUFBQyxPQUFPLENBQUMsRUFBRTtZQUNWLE1BQU0sSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDcEI7SUFDSCxDQUFDO0NBZ0hGO0FBaEpELHdCQWdKQyJ9